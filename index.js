const express = require('express');
const app = express();
const session = require('express-session')
require('dotenv').config()


app.use(session({
    secret: 'shaggy420',
    resave: false,
    saveUninitialized: true,
}))

app.get('/default', (req, res) =>{

})

app.listen(process.env.PORT, () => {console.log(`Server running on port ${process.env.PORT}`)})